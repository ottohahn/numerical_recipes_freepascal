UNIT numericalrecipes;
{
CREATED BY: OTTO HAHN HERRERA
DATE: 2023-04-07
PURPOSE: A COLLECTION OF NUMERICAL RECIPES IN FREEPASCAL TO BE USED FOR
NUMERICAL ANALYSIS, OR AS A BASE FOR MORE COMPLEX PROGRAMS. ALSO, IT IS A TEST
FOR DEBUGGING AND VALIDATING CHATGPT'S PROGRAMMING CAPABILITIES. SO, CHATGPT IS
A CO-AUTHOR.
CONTENTS:
PLANNED ROUTINES:
- VERY LARGE INTEGER ARITHMETHIC
- Square root calculations
- trigonometric function calculations
- base 10, base2, natural logarithms
- INTERPOLATION ROUTINES (FOR REAL NUMBERS)
- CURVE FITTING AND REGRESSION (FOR REAL NUMBERS)
- POLYNOMIAL SOLVING (FOR REAL NUMBERS)
- MATRIX SOLVERS
- NUMERICAL INTEGRATION
- NUMERICAL DIFFERENTIATION
}

INTERFACE

type
  TLargeInt = array of Integer;

function AddIntegers(x, y: TLargeInt): TLargeInt;


IMPLEMENTATION

function AddIntegers(x, y: TLargeInt): TLargeInt;
{
Function that adds two arbitrary length integers
}
var
  i, carry, sum: Integer;
  lenX, lenY, lenResult: Integer;
  addResult: TLargeInt;
begin
  // Get the length of the input arrays
  lenX := Length(x);
  lenY := Length(y);

  // Determine the length of the result array
  if lenX >= lenY then
    lenResult := lenX + 1
  else
    lenResult := lenY + 1;

  // Initialize the result array
  SetLength(addResult, lenResult);
  carry := 0;

  // Perform the addition
  for i := 0 to lenResult-1 do
  begin
    sum := carry;
    if i < lenX then
      sum := sum + x[i];
    if i < lenY then
      sum := sum + y[i];

    addResult[i] := sum mod 10;
    carry := sum div 10;
  end;

  // Remove leading zeros from the result array
  while (lenResult > 1) and (addResult[lenResult-1] = 0) do
    Dec(lenResult);
  SetLength(addResult, lenResult);

  // Reverse the result array to get the correct order of digits
  for i := 0 to lenResult div 2 - 1 do
  begin
    sum := addResult[i];
    addResult[i] := addResult[lenResult-1-i];
    addResult[lenResult-1-i] := sum;
  end;
  AddIntegers := addResult;
end;
    
END.

