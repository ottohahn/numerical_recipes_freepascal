# Contributing Guidelines

Thank you for considering contributing to our project! We welcome all 
contributions, big or small.

## Getting Started

1. Fork the project on GitLab
2. Clone your forked repository to your local machine
3. Install the project dependencies
4. Make your changes and test them thoroughly
5. Create a pull request

## Code Style

Please follow these code style guidelines when contributing to our project:

### Naming Conventions

- Use descriptive and meaningful names for variables, functions, and other identifiers
- Use PascalCase for type names and UpperCamelCase for function and procedure names
- Use camelCase for variable and parameter names
- Use all caps for constants

### Formatting

- Use 4 spaces for indentation
- Put spaces around operators and after commas

### Comments

- Use a master comment after the function definition to define inputs, outputs
  and overall behaviour or algorithms used.
- Use comments to explain the purpose of the code and any non-obvious behavior
- Use `//` for single-line comments and `(* ... *)` for multi-line comments

## Reporting Issues

If you encounter a bug or issue with the project, please create an issue on GitLab.

## Pull Requests

When creating a pull request, please include a brief description of your changes
and reference any related issues. All pull requests should be made against the
`main` branch.

We appreciate your contributions and thank you for your help in making our 
project better!
